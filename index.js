const FIRST_NAME = "Andreea";
const LAST_NAME = "Văcaru";
const GRUPA = "1085";

/**
 * Make the implementation here
 */

function numberParser(value) {
  // return Math.round(value);

  if (value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER) {
    return NaN;
  }

  // isNaN('bye-bye') e true
  if (isNaN(value)) {
    return NaN;
  } else {
    // daca e infinit
    if (!isFinite(value)) {
      return NaN;
    }

    if (value % 1 < 0.5) {
      return value - (value % 1);
    }

    if (value % 1 > 0.5) {
      return value + 1 - (value % 1);
    }

    if (value % 1 === 0.5) {
      return value + 0.5;
    }
  }
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  numberParser
};
